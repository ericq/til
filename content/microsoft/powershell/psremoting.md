---
title: "Powershell Remote"
date: 2018-10-25T12:06:26+02:00
description : ""
#draft: false
tags: ["powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Enable Powershell remote on Windows Core
On the remote endpoint enable 
```powershell
Enable-PSRemoting -Force
Set-NetFirewallRule -Name "WINRM-HTTP-In-TCP" -RemoteAddress Any
```
Mak sure the firewall profile is set to private or domain.


## PS remote outside domain
If the local machine is not inside the same domain you need to add the romote endpoint as a trusted host. MITM still posible.
On local machine add the remote endpoint in wsman
```powershell
# Add one item
Set-Item WSMan:\localhost\Client\TrustedHosts -Value "FQDN-or-IP"

# Add a next one if needed (see Get-Item)
Set-Item WSMan:\localhost\Client\TrustedHosts -Value "FQDN-or-IP" -Concatenate

#Then restart WinRM
Restart-Service WinRM
```
Test the connection
```powershell
Test-WsMan COMPUTER
```

## Remote Hyperv
https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/manage/remotely-manage-hyper-v-hosts

## copy files trough PS remote
Copy files over with pssession
```powershell
# Create a remote session
$session = New-PSSession -ComputerName 10.255.5.12 -Credential (Get-Credential)
# Copy the files (remote to local)
Copy-Item -FromSession $session -Path C:\temp\test.txt -Destination C:\Users\erik\Documents
# Or the other way (local to remote)
Copy-Item -ToSession $session -Path C:\Users\erik\Documents\test.txt -Destination C:\temp
# Close the session
$session | Remove-PSSession
```