---
title: "Powershell and Firewall"
date: 2018-10-25T11:49:01+02:00
description : ""
#draft: false
tags: ["powershell", "firewall"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Change Firewall Profile

Get active firewall profiles

```powershell
Get-NetConnectionProfile
```

List network adapters

```powershell
Get-NetAdapter
```

get network adapter profile (see current profile)

```powershell
Get-NetConnectionProfile -InterfaceIndex 6
```

Set profile on interface

```powershell
Set-NetConnectionProfile -InterfaceIndex 6 -NetworkCategory Private
```
