---
title: "Cheap Online Storage"
date: 2018-01-28T13:03:09+01:00
draft: false
tags: ["Storage", "Cloud Storage"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

* Backblaze B2 Cloud Storage
 https://www.backblaze.com/b2/cloud-storage.html
* Wasabi Hot Storage
 https://wasabi.com/
