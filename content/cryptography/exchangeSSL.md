---
title: "Exchange SSL"
date: 2020-03-26T11:20:35+02:00
description : ""
#draft: false
tags:
 - exchange
#- Development
#- Go
#- Powershell
#- Blogging
---

Tool

* [IIS Crypto ](https://www.nartac.com/Products/IISCrypto/)
  
Howto

* [Exchange TLS & SSL Best Practices
](https://techcommunity.microsoft.com/t5/exchange-team-blog/exchange-tls-038-ssl-best-practices/ba-p/603798#)
* [Exchange Server TLS guidance, part 1: Getting Ready for TLS 1.2
](https://techcommunity.microsoft.com/t5/exchange-team-blog/exchange-server-tls-guidance-part-1-getting-ready-for-tls-1-2/ba-p/607649#)
* [Exchange Server TLS guidance Part 2: Enabling TLS 1.2 and Identifying Clients Not Using It
](https://techcommunity.microsoft.com/t5/exchange-team-blog/exchange-server-tls-guidance-part-2-enabling-tls-1-2-and/ba-p/607761)
* [Exchange Server TLS guidance Part 3: Turning Off TLS 1.0/1.1
](https://techcommunity.microsoft.com/t5/exchange-team-blog/exchange-server-tls-guidance-part-3-turning-off-tls-1-0-1-1/ba-p/607898)