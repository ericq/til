---
title: "Veeam"
description : ""
#tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

{{% children depth="999" %}}

Recources
* https://www.veeam.com/veeam-free-online-training.html
* https://leanpub.com/vmce95unofficialstudyguide
* https://leanpub.com/masterveeamtricksvolume1
* https://www.veeambp.com/
* https://www.vccbook.io/

Products of Veeam Availability Suite
* Veeam Backup & Replicate
* Veeam One

Veeam Backup & Replicate Mandatory Components
* Backup Proxy
* Veeam Backup Server
* Backup Repository
* Standalone repository
* Scale-out Repository(Optional)

Veeam Backup & Replicate Optional Components
* Veeam Enterprise Manager
* WAN Accelerator
* Tape Server
* Guest Interaction Proxy
* Mount Server
* Console