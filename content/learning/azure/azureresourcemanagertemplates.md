---
title: "Azure Resource Manager (ARM) Templates"
date: 2019-05-28T08:08:40+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

Also see *Azure Resource Manager*.
Template can deploy multiple resources in a correct order. It will create depending resources first.
Microsoft has a list of [quickstart templates](https://azure.microsoft.com/en-us/resources/templates/).

## Deploy a ARM template

You can deploy a template by using its [public URL](https://portal.azure.com/#create/Microsoft.Template/uri/https%3a%2f%2fraw.githubusercontent.com%2fAzure%2fazure-quickstart-templates%2fmaster%2f101-vm-simple-windows%2fazuredeploy.json).

### Deploy a ARM template with powershell

Deploy a template to a resource group. Powershell will ask for the required parameters.

```powershell
New-AzResourceGroup -Name deploymentgroup -Location westeurope
$templateUri = "https://raw.githubusercontent.com/Azure/azure-quickstart-templates/master/101-vm-simple-windows/azuredeploy.json"
New-AzResourceGroupDeployment -Name rg9deployment1 -ResourceGroupName deploymentgroup -TemplateUri $templateUri
```

## Empty Template schema

The template [structure](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-authoring-templates).

```json
{
    "$schema": "http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
    "contentVersion": "",
    "parameters": {  },
    "variables": {  },
    "functions": [  ],
    "resources": [  ],
    "outputs": {  }
}
```

| Element | Required | Purpose |
|---|---|---|
| $schema | Yes | Location of the JSON schema file. |
| contentVersion | Yes | Version of the template file. |
| parameters | No | Values to customize the deployment. |
| variables | No | Values that are used in the JSON. |
| functions | No | User defined functions that are available withing the template. |
| resources | Yes | Resource types that are deployed or updated. |
| outputs | No | Values that are returned after deployment. |

### Parameters

The [parameters](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-authoring-templates#parameters) are values that can be specified when running the deployment.
You are limited tot 256 parameters.

```json
"parameters": {
  "adminUsername": {
    "type": "string",
    "metadata": {
      "description": "Username for the Virtual Machine."
    }
  },
  "adminPassword": {
    "type": "securestring",
    "metadata": {
      "description": "Password for the Virtual Machine."
    }
  },
    "storageSKU": {
    "type": "string",
    "allowedValues": [
      "Standard_LRS",
      "Standard_ZRS",
      "Premium_LRS"
    ],
    "defaultValue": "Standard_LRS",
    "metadata": {
      "description": "The type of replication to use for the storage account."
    }
  }  
}
```

### Template variables

In the variable element you define variables you want to us multiple times inside the template.

```JSON
"variables": {
  "nicName": "myVMNic",
  "addressPrefix": "10.0.0.0/16",
  "subnetName": "Subnet",
  "subnetPrefix": "10.0.0.0/24",
  "publicIPAddressName": "myPublicIP",
  "virtualNetworkName": "MyVNET"
}
```