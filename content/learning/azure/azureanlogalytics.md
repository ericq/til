---
title: "Azure log Analytics"
date: 2019-07-24T16:31:05+02:00
description : ""
#draft: false
tags: ["azureanalytics"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

## Resources

* [Get started with Log Analytics](https://docs.microsoft.com/en-us/azure/azure-monitor/log-query/get-started-portal)
* [Analyze Log Analytics data in Azure Monitor](https://docs.microsoft.com/en-us/azure/azure-monitor/log-query/log-query-overview)
* [Alert Management solution in Azure Log Analytics](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/alert-management-solution)
* [Log Analytics Query Examples](https://github.com/MicrosoftDocs/LogAnalyticsExamples/tree/master/log-analytics)
