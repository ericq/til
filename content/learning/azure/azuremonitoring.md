---
title: "Azure Monitoring"
date: 2019-06-06T19:14:27+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

* **Monitor and Visualize Metric** - Monitor numerical values to see the health, operation and performance.
* **Query and Analyze Logs** - Logs are activity logs, diagnostics logs and telemetry from monitoring solutions. Analytics queries help with troubleshooting and visualizations.
* **Alert and Actions** - Alert when a condition is reached and take corrective automated actions based on triggers from metrics and logs.
* Action Groups - Named groups of notifications and actions for alerts

## Azure Monitor Service

Monitor your environment to determine the performance, health and availability of your environment by collecting and analyzing data.

* **Metrics** are numerical values.
* **Logs** contain different kinds of data organized into records with different sets of properties.

### Metrics

### Log Data

Log data collected by Azure Monitoring is stored in a Log Analytics workspace which is based on [Azure Data Explorer](https://docs.microsoft.com/en-us/azure/data-explorer/). It uses the [Kusto query language](https://docs.microsoft.com/en-us/azure/kusto/query/) to retrieve, consolidate and analyze collected data.

#### Data Types

Azure Monitor can collect data from various sources and categorieze these in different layers, from the application all the way down to the OS and Azure platform. This are the layers:

* **Application monitoring data** - Data about the performance and functionality of the code of the program.
* **Guest OS monitoring data** - Data about your running operating system. This could be in Azure, another cloud or on-premise. This can be done via Azure Diagnostic extension.
* **Azure resource monitoring data** - Data about the operation of Azure
* **Azure subscription monitoring data** - Data about the operation and management of an Azure subscription, as well as data about the health and operation of Azure itself.
* **Azure tentant monitoring data** - Data about the operation pf tenant-level Azure services, like Azure AD.

## Azure Monitor Alerts

Create alert actions when something goes wrong. Monitor Alerts are seperated in a different views. The active alerts, alert rules and alert actions. Alerts consists of alert rules, action groups, and monitor conditions. The alert rule is a definition of when to trigger a alert. This can be from metrics, logs, activity logs, log analytics or application insights. The alert action is an action to do when a alert is raised. Send a e-mail or SMS, start an Azure Function, LogicApp, Webhook Automation Runbook or push to a ITSM service.

An alert can have a state of New when started, a state of Acknowledged can be set by an administrator. The state of Closed will be set if the problem is resolved.

The alert rule consists of the follow items:

* **Target source** - This defines the scope and available signals. Different resources will show different Signals.
* **Signal** - Different signal sources like Metrics, Activity log, Application insight or log.
* **Criteria Condition** - This is a combination of the signal and logic on a target like Percentage CPU > 70%; Server Response Time > 4 ms; and Result count of a log query > 100.
* **Alert name** - Name of the alert
* **Alert Description** 
* **Severity** - Severity can range from 0 to 4.
* **Action** - Action to do when a alert is fired, defined by an actiong group.

Alert can monitor the follow signal but is not limited to only these.

* Metric values
* Log search queries
* Activity Log events
* Health of the underlying Azure platform
* Tests for web site availability

Action groups can be used for Azure monitoring alerts. One action group can be used on multiple alert rules. When a person his email or SMS is added to an action group, the person will receive al alert of beeing added to the group. The follow alert items can be used:

* **Email** - Email can be send to up to 1000 adresses.
* **ITSM** - You may have up to 10 ITSM actions in an Action Group ITSM Action requires an ITSM Connection.
* **Logic App** - You may have up to 10 Logic App actions in an Action Group.
* **Function App** - The function keys for Function Apps configured as actions are read through the Functions API.
* **Runbook** - You may have up to 10 Runbook actions in an Action Group.
* **SMS** - You may have up to 10 SMS actions in an Action Group.
* **Voice** - You may have up to 10 Voice actions in an Action Group. US only now.
* **Webhook** - You may have up to 10 Webhook actions in an Action Group. Retry logic - The timeout period for a response is 10 seconds. The webhook call will be retried a maximum of 2 times when the following HTTP status codes are returned: 408, 429, 503, 504 or the HTTP endpoint does not respond. The first retry happens after 10 seconds. The second and last retry happens after 100 seconds.

You may have up to 10 Azure app actions in an Action Group. At this time the Azure app action only supports ServiceHealth alerts.

## Activity Log

The activity is a subscription log that provides insight into subscription-level events.This includes a range of data, from Azure Resource Manager operational data to updates on Service Health events. With the Activity Log, you can determine the ‘what, who, and when’ for any write operations (PUT, POST, DELETE) taken on the resources in your subscription. You can also understand the status of the operation and other relevant properties. Through activity logs, you can determine:

* What operations were taken on the resources in your subscription.
* Who started the operation.
* When the operation occurred.
* The status of the operation.
* The values of other properties that might help you research the operation.

Activity logs are kept for 90 days. You can query for any range of dates, as long as the starting date isn't more than 90 days in the past. You can retrieve events from your Activity Log using the Azure portal, CLI, PowerShell cmdlets, and Azure Monitor REST API.

## Azure Advisor

Advisor is a personalized cloud consultant that helps you follow best practices to optimize your Azure deployments. It analyzes your resource configuration and usage telemetry and then recommends solutions that can help you improve the cost effectiveness, performance, high availability, and security of your Azure resources.

The Advisor cost recommendations page helps you optimize and reduce your overall Azure spend by identifying idle and underutilized resources.

Advisor provides recommendations for virtual machines, availability sets, application gateways, App Services, SQL servers, and Redis Cache.

## Resources

* [Azure Monitor Overview](https://docs.microsoft.com/en-us/azure/azure-monitor/overview)
* [Azure Monitor data platform](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/data-platform)
* [Azure Monitor Alert overview](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/alerts-overview)
* [Cost Management Report](https://docs.microsoft.com/en-us/azure/cost-management/use-reports)
* [Setup Billing Alerts](https://docs.microsoft.com/en-us/azure/billing/billing-getting-started)
* [Create Alerts in Azure Monitor](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/alerts-metric)
* [Collect and consume log data from your Azure resources](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/diagnostic-logs-overview#resource-diagnostic-settings)
* [Alerts with dynamic thresholds in Azure Monitor](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/alerts-dynamic-thresholds)
* [Azure action groups](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/action-groups)
* [Log alerts in Azure Monitor](https://docs.microsoft.com/en-us/azure/azure-monitor/platform/alerts-unified-log)