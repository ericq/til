---
title: "Azure Subscriptions"
date: 2019-06-05T19:55:30+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

* Azure subscription
* Enterprise agreement (EA)
* Management Groups
* Billing alerts

## Azure subscriptions

A Azure subscription is a unit in which you can run services. Billing for Azure services is done on a per-subscription basis. If your account is the only account associated with a subscription, then you are responsible for billing.

Subscriptions have accounts. An Azure account is an identity like Azure Active Directory account or Microsoft account.

### Azure account

An Azure account determines how Azure usage is reported and who the Account Administrator is. Accounts and subscriptions are created in the Azure Account Center. The person who creates the account is the Account Administrator for all subscriptions created in that account. That person is also the default Service Administrator for the subscription.

There are three roles related to Azure accounts and subscriptions:

* **Account Administrator** - Authorized to access the Azure Account Center (Create and cancel subscription, change billing or change service administrator). This role has full control over the subscription and is is responsible for the billing. Max 1 per subscription.
* **Service Administrator** - Authorized to access the Azure management platform for all subscriptions in the account. By default, same as the Account Administrator when a subscription is created. This role has control over all the services in the subscription. Max 1 per subscription.
* **Co-Administrator** - Same as Service Administrator but can’t change the association of subscriptions to Azure directories. Max 200 per subscription.

#### Account administrator

The Account administrator for a subscription is the only person who can access the Azure Account center. The Account administrator does not have any other access to services inside that subscription. They need additional Service administrator or Co-administrator rights to manage the services. For security reasons, the Account Administrator for a subscription can only be changed with a call to Azure support. The Account administrator can reassign the Service administrator rights in the Azure Account center.

#### Service and Co-administrator

The Service Administrator is the first Co-Administrator for a subscription. Like other Co-Administrators, the Service Administrator has management access to cloud resources using the Azure Management Portal, as well as tools like Visual Studio, other SDKs, and command line tools like PowerShell. The Service Administrator can also add and remove other Co-Administrators. The Service Administrator is the only user authorized to change a subscription’s association with a directory in the Azure Management Portal.

Account Administrators using a Microsoft account must log in every 2 years (or more frequently) to keep the account active. Inactive accounts are cancelled, and the related subscriptions removed. There are no login requirements if using a work or school account.

### Getting a Azure subscription

You can get a Azure subscription via an Enterprise agreement, Microsoft reseller, Microsoft partner of personal (free) account.

* **Enterprise agreements** - A Enterprise agreement customer make a upfront monetary commitment with Microsoft. 
* **Reseller** - This allowes you to buy Azure credits via an [Open license program](https://azure.microsoft.com/en-us/offers/ms-azr-0111p/).
* **partners** - Azure applications deployed and managed by external partners, or Azure Expert Managed Services Providers.
* **Personal (free) Account** - For personal usage.

### Subscription Usage

Azure offers free and paid subscription options. The most commonly used subscriptions are:

* **Free** - A free trail with a $200 fee and [**free Azure services**](https://azure.microsoft.com/en-us/free/).
* **Pay-As-You-Go** - (PAYG) subscription charges you monthly for the services you used.
* **Enterprise Agreement** - provides flexibility to buy cloud services and software licenses under one agreement,with discounts for new licenses and Software Assurance. Targeted at enterprise-scale organizations.
* **Student** - Azure for Students includes $100 in Azure credits to be used within the first 12 months plus select free services.

## Resource limits

Azure provides an overview tho see the number of resources used in *Usage + Quotas*. In this overview you also see the default limits. If you want to use more than the [default limits](https://docs.microsoft.com/en-us/azure/azure-subscription-service-limits), you need to put a request via a form.

## Management Groups

[Management groups](https://docs.microsoft.com/en-us/azure/governance/management-groups/index) are the new way to manage multiple subscriptions. With management groups you can manage access, policies, and compliance for those subscriptions. You can place subscriptions and management groups inside a management group. The policies is applied to all management groups, subscriptions, and resources under that management group. 

* Set access control (RBAC).
* Set policies (Compliance).
* Applied policies or access can not be altered in lower groups.
* Create management groups with the Azure portal, Powershell and the CLI.
* You can not create management groups with Resource Manager templates.
* A management group tree can be 6 levels deep.

{{<mermaid align="left">}}
graph TD
A[fa:fa-users<BR>Tenant root<BR>Group]
A -->B[fa:fa-users<BR> IT]
A -->C[fa:fa-users<BR> Marketing]
A -->D[fa:fa-users<BR> HR]
B -->E[fa:fa-users<BR> Production]
B -->F[fa:fa-users<BR> Development]
E -->G[fa:fa-key<BR> EA Subscription]
E -->H[fa:fa-key<BR> EA Subscription]
D -->I[fa:fa-key<BR> Free Trail]
A -->J[fa:fa-key<BR> EA Subscription]
{{< /mermaid >}}

The management group has two field when creating.

* **Management Group ID** - A non editable unique identifier.
* **The Display Name** - A editable optional field.

Powershell command to create management groups.

```powershell
New-AzManagementGroup -GroupName 'Contoso' -DisplayName 'Contoso Development'
```

## Azure enterprise enrolment

{{<mermaid align="left">}}
graph LR;
  A[Azure Enterprise <BR>Enrolment]
  A -->|1 to <BR>Many| B[Departments]
  B -->|1 to  <BR>Many| C{Accounts}
  C -->|1 to  <BR>Many| D{Subscriptions}
  D -->|One| E[Resource one]
  D -->|Two| F[Resource two]
{{< /mermaid >}}

* Azure Enterprise - `http://ea.azure.com`
  * Departments
    * Accounts - `http://account.azure.com`
      * **Subscriptions** - `http://portal.azure.com`
        * Resource Groups
          * Resources

## Billing

* **Billing alerts** are available from the Azure Account portal. You can set a total of 5 billing alerts per subscription. And send alerts to two e-mail addresses.
* **[Reservations](https://docs.microsoft.com/en-us/azure/billing/billing-save-compute-costs-reservations)** helps saving money by pre-paying one or two years for a service like virtual machine, SQL Database compute capacity, Azure Cosmos DB throughput. Reservation will give a discount up to 72% on pay-as-you-go prices. You can [cancel a reservation](https://docs.microsoft.com/en-us/azure/billing/billing-azure-reservations-self-service-exchange-and-refund) with a 12% early termination fee.
* **[Budgets](https://docs.microsoft.com/en-us/azure/cost-management/tutorial-acm-create-budgets)** help you plan with accountability. You can also start alerts and **Azure automation runbooks** based on at threshold.
* **[Cost analysis](https://docs.microsoft.com/en-us/azure/cost-management/quick-acm-cost-analysis)** lets you analyze costs.
* **Plan pricing** Use the [Azure pricing calculator](https://azure.microsoft.com/en-us/pricing/calculator/) to plan costs.

## Resources

* [Azure management groups](https://docs.microsoft.com/en-us/azure/governance/management-groups/index)
* [Azure subscription administrators](https://docs.microsoft.com/en-us/azure/billing/billing-add-change-azure-subscription-administrator)
* [Prevent unexpected charges with Azure billing and cost management](https://docs.microsoft.com/en-us/azure/billing/billing-getting-started)
