---
title: "Azure VM Powershell create"
date: 2019-05-29T12:03:16+02:00
description : ""
#draft: false
tags: ["azure", "powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Quickly create a new VM

```powershell
New-AzVm `
    -ResourceGroupName "myResourceGroup" `
    -Name "myVM" `
    -Location "East US" `
    -VirtualNetworkName "myVnet" `
    -SubnetName "mySubnet" `
    -SecurityGroupName "myNetworkSecurityGroup" `
    -PublicIpAddressName "myPublicIpAddress" `
    -OpenPorts 80,3389
```

## Specify

```powershell
$cred = Get-Credential

# Create the initial configuration
$vm = New-AzVMConfig -VMName myVM -VMSize Standard_Ds1_v2

# Set the operating system
$vm = Set-AzVMOperatingSystem `
-VM $vm `
-Windows `
-ComputerName myVM `
-Credential $cred `
-ProvisionVMAgent -EnableAutoUpdate

# Set the image information
$vm = Set-AzVMSourceImage `
-VM $vm `
-PublisherName MicrosoftWindowsServer `
-Offer WindowsServer `
-Skus 2016-Datacenter `
-Version latest

# Create the VM
New-AzVM -ResourceGroupName myResourceGroupVM -Location EastUS -VM $vm
```
