---
title: "Azure VM machine extension"
date: 2019-06-04T13:59:48+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

Azure virtual machine (VM) extensions are small applications that provide post-deployment configuration and automation tasks on Azure VMs. For example, if a virtual machine requires software installation, anti-virus protection, or to run a script inside of it, a VM extension can be used.

Azure VM extensions can be:

* Managed with Azure CLI, PowerShell, Azure Resource Manager templates, and the Azure portal.
* VM configuration, you can use Powershell DSC (Desired State Configuration), Chef, Puppet and Custom Script Extensions to install VM configuration agents and configure your VM.
* AV products, such as Symantec, ESET.
* Configure monitoring of a VM with the Microsoft Monitoring Agent VM extension.
* VM vulnerability tool, such as Qualys, Rapid7, HPE.

Virtual machine extensions and features for [Windows](https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/features-windows) and [Linux](https://docs.microsoft.com/en-us/azure/virtual-machines/extensions/features-linux).

### Custom Script Extensions

Custom Script Extension (CSE) can be used to automatically launch and execute virtual machine customization tasks post configuration. Your script extension may perform very simple tasks such as stopping the virtual machine or installing a software component or even a more complex script.

* Timeout - Custom script extensions have a timeout of 90 minutes to run. If the script is not finished by that time the Azure status will go to timeout.
* Dependencies - If the script requires network or storage access, make sure the content is available.
* Failure events - Be sure to account for any errors that might occur when running your script. For example, running out of disk space, or security and access restrictions. What will the script do if there is an error?

### Desired State Configuration

[Desired State Configuration](https://docs.microsoft.com/en-us/powershell/dsc/overview/overview) (DSC) is a management platform in Windows PowerShell that enables deploying and managing configuration data for software services and managing the environment in which these services run.

```powershell
configuration IISInstall
{
  Node “localhost”
  {
    WindowsFeature IIS
    {
      Ensure = “Present”
      Name = “Web-Server”
    } 
  } 
 }
```