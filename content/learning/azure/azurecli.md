---
title: "Azure CLI"
date: 2019-05-27T09:59:38+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

* CLI
* Bash

## login
```bash
az login
```

## Find commands
Intelligent command search
```bash
az find blob

az find 'az monitor activity-log list'
```

## Get help
Find commands 
```bash
az --help
```

Get info on a command
```bash
az storage blob --help
```

## Output formats
Output formats
* json
* jsonc
* yaml
* table
* tsv

```bash
az vm list --out table
```

## Filter Output
Use JMESPath to query a request
```bash
az group list --query "[?name == '<rg name>']"

az group list --query "[?location == 'westeurope']"
```
See http://jmespath.org/

## Resources