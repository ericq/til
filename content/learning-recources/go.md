---
title: "Golang"
date: 2020-10-31T11:28:57+02:00
description : ""
#draft: false
tags:
#- go
#- Development
#- Go
#- Powershell
#- Blogging
---

* [Golang main site](https://golang.org/)
* [Golang main docs](https://golang.org/doc/)
* [Tour of Golang](https://tour.golang.org/welcome/1)


## Video

* [YT: justforfunc: Programming in Go](https://www.youtube.com/c/JustForFunc/featured)
* [YT: Learn To Code](https://www.youtube.com/c/toddmcleod-learn-to-code)
* [YT: Failing Forward](https://www.youtube.com/watch?v=OSPNUKoN81o&list=PLq9Ra239pNZC0MgMN4j6ZiPHv_c0UPnBX)
* [YT: FreeCodeCamp Learn Go Programming - Golang Tutorial for Beginners](https://www.youtube.com/watch?v=YS4e4q9oBaU&t)
* [YT: Ardan Labs](https://www.youtube.com/channel/UCCgGRKeRM1b0LTDqqb4NqjA/playlists)
* [Udemy: Go Bootcamp: Learn to Code with Golang](https://www.udemy.com/course/learn-go-the-complete-bootcamp-course-golang/?referralCode=5CE6EB34E2B1EF4A7D37)
* [Pluralsight: Go Core Path](https://www.pluralsight.com/paths/go-core-language)


## Course and Tutorial series

* [Golang Bot tutorial series](https://golangbot.com/learn-golang-series/)
* [Go by Example](https://gobyexample.com/)* [Learn Go with Tests](https://github.com/quii/learn-go-with-tests)
* [Essential Go](https://essential-go.programming-books.io/)
* [Go Bootcamp](http://www.golangbootcamp.com/book)
* [Go 101](https://go101.org/article/101.html) - Intermediate?
* [TutorialsPoint: Go Tutorial](https://www.tutorialspoint.com/go/index.htm)
* [Learn Web Programming in Go by Examples](https://gowebexamples.com/)
* [Learning Cloud Native Go](https://learning-cloud-native-go.github.io/)
* [Digital Ocean: How To Code in Go eBook](https://www.digitalocean.com/community/books/how-to-code-in-go-ebook)
* [The Go Workshop](https://courses.packtpub.com/courses/go?utm_source=github&utm_medium=repository&utm_campaign=1838647945&utm_term=GO&utm_content=The%20Go%20Workshop)
* [Web dev golang anti textbook](https://github.com/thewhitetulip/web-dev-golang-anti-textbook/)
* [Build Web Application with Golang](https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/)
* [GolangTraining](https://github.com/GoesToEleven/GolangTraining)
* [Go-learning-links](https://gitlab.com/zendrulat123/go-learning-links/blob/master/README.md)


## Books

* [The Little Go Book](https://www.openmymind.net/The-Little-Go-Book/)
* [An Introduction to Programming in Go](https://www.golang-book.com/)

## Exercises
* [Gophercises](https://gophercises.com/)
* [Exercism](https://exercism.io/)
* [Algorithms with Go](https://algorithmswithgo.com/)
* [Project Euler](https://projecteuler.net/)


## Learning lists

* [Go Developer Roadmap](https://github.com/Alikhll/golang-developer-roadmap) - List of what to learn at what point
* [GoBooks](https://github.com/dariubs/GoBooks)
* [Golang Books](https://github.com/golang/go/wiki/Books)
* [Golang official Learn](https://github.com/golang/go/wiki/Learn)
* [Gdocs with courses, trainings and resources](https://docs.google.com/document/d/18-0u5CvNIr83oOfMXPoM4klVFASXGl3Vvua1wBGMIoQ/edit)
* [Dave Cheney: Resources for new Go programmers](https://dave.cheney.net/resources-for-new-go-programmers)
* [ReactDOM: Learn Go resouces](https://reactdom.com/go)
* [Gdocs Go Advocates](https://docs.google.com/document/d/1Zb9GCWPKeEJ4Dyn2TkT-O3wJ8AFc-IMxZzTugNCjr-8/edit)
* [Learn Programming: Learn Go](https://go.learnprogramming.tips/)
* [Awesome Go](https://github.com/avelino/awesome-go#tutorials)
* [Reddit: What are the best resources to learn Go from scratch](https://www.reddit.com/r/golang/comments/5djil6/what_are_the_best_resources_to_learn_go_from/)


## Others

* [Games With Go](https://gameswithgo.org/)
* [My Experience with Learning Golang](https://dev.to/pmihaylov/my-experience-with-learning-golang-5565)
* [Getting started with Go guide](https://dominicstpierre.com/getting-started-with-go-guide)
* [Codementor: Go Tutorials and Insights](https://www.codementor.io/community/topic/go)
