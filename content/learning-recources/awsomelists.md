---
title: "Awsome lists"
date: 2018-06-05T13:14:00+02:00
description : ""
#draft: false
tags: ["Notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

* https://github.com/kahun/awesome-sysadmin/blob/master/README.md#editors
* https://github.com/clowwindy/Awesome-Networking/blob/master/README.md
* https://github.com/Kickball/awesome-selfhosted/blob/master/README.md
* https://github.com/briatte/awesome-network-analysis/blob/master/README.md
* https://github.com/cisco-ie/awesome-network-programmability/blob/master/readme.md
* https://github.com/cisco-ie/awesome-network-programmability/blob/master/readme.md
* https://github.com/enaqx/awesome-pentest/blob/master/README.md