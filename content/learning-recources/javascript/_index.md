---
title: "Javascript"
description : ""
#tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

{{% children depth="999" %}}

## Javascript beginning
* [x] http://jsforcats.com/
* [ ] https://www.rithmschool.com/courses/javascript
* [ ] https://javascript.info/
* [ ] https://github.com/getify/You-Dont-Know-JS
* [ ] https://ydkjs-exercises.com/
* [ ] http://eloquentjavascript.net/
* [ ] https://www.freecodecamp.org/
* [ ] https://developer.mozilla.org/en-US/docs/Web/JavaScript
* [ ] https://watchandcode.com/
* [ ] http://www.crunchzilla.com/code-maven
* [ ] https://nostarch.com/javascriptforkids
* [ ] https://flatironschool.com/free-courses/learn-javascript

## Extra Reads
* http://callbackhell.com

## NodeJS / Express / Javascript / web fullstack

* [ ] https://www.udemy.com/the-web-developer-bootcamp/

## Udacity series - uitzoeken
* Beginner
  * [x] https://eu.udacity.com/course/intro-to-javascript--ud803
  * https://eu.udacity.com/course/html5-canvas--ud292
  * https://eu.udacity.com/course/cs101
* Intermediate
  * https://eu.udacity.com/course/web-tooling-automation--ud892
  * https://eu.udacity.com/course/object-oriented-javascript--ud015
  * https://eu.udacity.com/course/intro-to-ajax--ud110
  * https://eu.udacity.com/course/intro-to-jquery--ud245
  * https://eu.udacity.com/course/javascript-testing--ud549
  * https://eu.udacity.com/course/asynchronous-javascript-requests--ud109
  * https://eu.udacity.com/course/website-performance-optimization--ud884
* Advanced
  * https://eu.udacity.com/course/javascript-design-patterns--ud989
  * https://eu.udacity.com/course/front-end-frameworks--ud894
  * https://eu.udacity.com/course/javascript-promises--ud898
  * https://eu.udacity.com/course/es6-javascript-improved--ud356



### JavaScript things to do and check
* [x] https://codeburst.io/javascript-the-keyword-this-for-beginners-fb5238d99f85
* https://codeburst.io/javascript-arrow-functions-for-beginners-926947fc0cdc
* https://codeburst.io/javascript-what-the-heck-is-a-callback-aba4da2deced
* https://codeburst.io/javascript-what-the-heck-is-an-immediately-invoked-function-expression-a0ed32b66c18
* https://codeburst.io/javascript-for-beginners-the-new-operator-cee35beb669e
* https://codeburst.io/javascript-learn-regular-expressions-for-beginners-bb6107015d91
* https://codeburst.io/javascript-template-literals-tag-functions-for-beginners-758a041160e1
* https://codeburst.io/javascript-the-spread-operator-a867a71668ca
* https://codeburst.io/javascript-what-is-short-circuit-evaluation-ff22b2f5608c
* https://codeburst.io/javascript-what-is-the-ternary-operator-c819af8a7f6c
* https://codeburst.io/javascript-why-does-3-true-4-and-7-other-tricky-equations-9dd13cb2a92a
* https://codeburst.io/javascript-whats-the-difference-between-null-undefined-37793b5bfce6
* https://codeburst.io/learn-and-understand-recursion-in-javascript-b588218e87ea
* https://codeburst.io/understand-closures-in-javascript-d07852fa51e7
* https://developers.google.com/web/tools/chrome-devtools/