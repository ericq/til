---
title: "Regular Expressions"
description : ""
tags: ["regex"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

## Regex testing

* https://regexr.com/
* https://regex101.com/
* https://extendsclass.com/regex-tester.html

## Learning resources

* https://shiffman.net/a2z/regex/ - video
* https://regexone.com/ - interactive site
* http://regextutorials.com/ - interactive site
* https://www.tldp.org/LDP/abs/html/regexp.html
* https://regexcrossword.com/
* https://www.regular-expressions.info/tutorial.html
* https://qntm.org/files/re/re.html
* https://github.com/ziishaned/learn-regex
* http://www.rexegg.com/
* https://blog.codinghorror.com/regular-expressions-now-you-have-two-problems/
* https://blog.patricktriest.com/you-should-learn-regex/
* https://projects.lukehaas.me/regexhub/
* http://eloquentjavascript.net/09_regexp.html
* https://media.cheatography.com/storage/thumb/davechild_regular-expressions.750.jpg

### Sed

* http://www.grymoire.com/Unix/Sed.html
