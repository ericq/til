---
title: "Learn Piano"
date: 2020-07-14T21:42:42+02:00
description : ""
#draft: false
tags:
#- piano
#- Development
#- Go
#- Powershell
#- Blogging
---

## lists

* [Reddit LearnPiano](https://www.reddit.com/r/pianolearning/wiki/index) and [Resources](https://www.reddit.com/r/pianolearning/wiki/beginners)
* [Reddit Piano FAQ](https://www.reddit.com/r/piano/wiki/faq)

## Sites

* [PianoLessons.com](https://www.pianolessons.com/) Free

## Youtube

* [Andrew Furmanczyk - How to Play Piano](http://www.howtoplaypiano.ca/learn-how-to-play-piano)
* [How To Play Piano for Beginners, Lesson 1](https://www.youtube.com/playlist?list=PLpOuhygfD7QnP46wUgQudOySX_z2UOhXs)

## Apps

* [TuneUpgrade](https://www.tuneupgrade.com/) free
* [Synthesia](https://synthesiagame.com/) $

## Course Book

* [Alfred's Basic Adult All-in-One Course, Book 1](https://www.amazon.com/Alfreds-Basic-Adult-All-Course-ebook/dp/B017OBN7CK/ref=sr_1_1?dchild=1&keywords=alfreds+basic+piano+adults&qid=1586624941&sr=8-1)
* [MusicTheory.net](https://www.musictheory.net/lessons)

## Sheets

* [IMSLP](https://imslp.org/wiki/Main_Page)
  