---
title: "Fantasy Music"
date: 2020-03-03T21:01:33+02:00
description : ""
#draft: false
tags:
- music
---

Music for fantasy moments. While reeding or playing DnD.

* [Ultimate RDP sets](https://soundcloud.com/ultimaterpg/sets) Soundcloud
* https://www.reddit.com/r/DnDPlaylist/comments/m4ys5m/look_no_more_for_immersive_soundscapes_heres_all/
* https://www.reddit.com/r/DnD/comments/595zc6/my_curated_spotify_playlists_i_use_for_dd/
* https://www.reddit.com/r/DnDPlaylist/comments/fcxv22/for_my_cake_day_here_is_gift_to_you_of_a_bunch_of/
* https://www.reddit.com/r/DnDPlaylist/comments/eyvkbh/figured_id_share_my_big_list_of_playlists_ive_put/
* https://www.reddit.com/r/DnDPlaylist/comments/esz4o0/variety_of_playlists_ive_created_for_my_gaming/
* https://www.reddit.com/r/DnDPlaylist/comments/m4k6p4/very_rare_neverending_rpg_music_spotify_playlists/
* https://www.reddit.com/r/DnDPlaylist/comments/lrciyr/10_spotify_dnd_playlists/
* https://www.reddit.com/r/DnDPlaylist/comments/lpvv9j/high_fantasy_rpg_soundtrack/


Spotify playlist test
{{< rawhtml >}}
<iframe src="https://open.spotify.com/embed/playlist/0wVZEi5U5Oj1QhV81ZZgCG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
{{< /rawhtml >}}