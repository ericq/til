---
title: "Synthesizer"
date: 2020-07-12T17:05:11+02:00
description : ""
#draft: false
tags:
#- synthesizer
#- Development
#- Go
#- Powershell
#- Blogging
---

Resources on learning about synthesizers. See also music theory.

* [Basics of Synthesis and Sound design.](https://medium.com/@kusekiakorame/basics-of-synthesis-and-sound-design-a-beginners-guide-9c3d0314c6d5)
* [Ableton Learning Synths online](https://learningsynths.ableton.com/)
* [TATS Synthesizer game](https://synthgame-c2436.firebaseapp.com/) - Online synt and learning game.
* [Learn about Waveforms](https://pudding.cool/2018/02/waveforms/)
* [Syntorial](https://www.syntorial.com/)
* [Synth Secrets](https://www.soundonsound.com/series/synth-secrets) Articles
* [Reverb Machine](https://reverbmachine.com/articles) - Deconstructing songs and patches
* [Creating Sounds from Scratch](https://books.google.co.uk/books/about/Creating_Sounds_from_Scratch.html?id=BYeuDQAAQBAJ&source=kp_book_description&redir_esc=y) Book
* [Coursera: Creating Sounds for Electronic Music](https://www.coursera.org/learn/music-synthesizer)
* [ADSR Envelopes: How to Build The Perfect Sound](https://blog.landr.com/adsr-envelopes-infographic/) - Infographic
* [DX7 - The Complete DX7](http://yates.ca/dx7/The%20Complete%20DX7/The%20Complete%20DX7.pdf)

Youtube

* [Intro to Synthesis Part 1 - The Building Blocks of Sound & Synthesis](https://www.youtube.com/watch?v=atvtBE6t48M) - [More](https://www.matrixsynth.com/2012/01/intro-to-synthesis-part-1-building.html)
* [SECRETS OF ANALOG AND DIGITAL SYNTHESIS](https://www.youtube.com/watch?v=tivES-sjHc4&t=644s)


## Frequency Modulation Synthesis

* [Manny's Modulation Manifesto: Intro to FM Synthesis](https://yamahasynth.com/synths/basics-of-fm-synthesis)



Other things
* [Musical Chord Progression Arpeggiator](https://codepen.io/jakealbaugh/full/qNrZyw)
* 